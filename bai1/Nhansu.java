package javaapplication2;

import java.util.Scanner;

public abstract class Nhansu {

    protected int ma;
    protected String ten;
    protected float luong;
    protected int gioiTinh;

    public int getMa() {
        return ma;
    }

    public void setMa(int ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public float getLuong() {
        return luong;
    }

    public void setLuong(float luong) {
        this.luong = luong;
    }

    public enum GioiTinh {

        NAM(1, "Nam"), NU(2, "Nu");  
        int key;
        String value;

        private GioiTinh(int key, String value) {
            this.key = key;
            this.value = value;
        }

    }

    public void nhap() {
        System.out.println("Nhap ten");
        ten = new Scanner(System.in).nextLine();
        System.out.println("Nhap gt (Nhap 1: Nam, Nhap 2: Nu): ");
        gioiTinh = new Scanner(System.in).nextInt();
        System.out.println("Nhap luong");
        luong = new Scanner(System.in).nextFloat();

    }

    public void in() {
        System.out.println("Ma: " + ma);
        System.out.println("Ho ten: " + ten);
        GioiTinh gioiTinh = null;
        if (gioiTinh.NAM.key == this.gioiTinh) {
            System.out.println("Gioi tinh: Nam");
        } else {
            System.out.println("Gioi tinh: Nu");
        }
        System.out.println("Luong: " + luong);
    }

    public abstract float thunhap();



}
