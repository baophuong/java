
package javaapplication2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DanhSachQuanLy {
    List<Quanly> dsql = new ArrayList<>();
    int n;

    public List<Quanly> getDsql() {
        return dsql;
    }

    public void setDsql(List<Quanly> dsql) {
        this.dsql = dsql;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }
    
     public  void Nhap() {
        boolean check = true;
        int id;
        for (int i = 0; i < n; i++) {
            System.out.println("Quan ly thu " + (i + 1));
            do {
                System.out.println("Nhap vao id: ");
                id = new Scanner(System.in).nextInt();
                for (int j = 0; j < dsql.size(); j++) {
                    if (dsql.get(j).getMa() == id) {
                        check = false;
                        break;
                    }
                }
                if (check == false) {
                    System.out.println("Ma da bi trung !!!");
                }
            } while (check == false);
            Quanly ql = new Quanly();
            ql.setMa(id);
            ql.nhap();
            dsql.add(ql);
        }

    }
    
       public void Xuat() {
        for (int i = 0; i < dsql.size(); i++) {
            dsql.get(i).in();
        }
    }
}
