package javaapplication2;

import java.util.Scanner;

public class Vanphong extends Nhansu {

    private float hesoluong;

    public float getHesoluong() {
        return hesoluong;
    }

    public void setHesoluong(float hesoluong) {
        this.hesoluong = hesoluong;
    }

    @Override
    public void nhap() {
        super.nhap();
        System.out.println("He so luong");
        hesoluong = new Scanner(System.in).nextFloat();
    }

    @Override
    public void in() {
        super.in();
        System.out.println("He so luong: " + hesoluong);
        System.out.println("Thu nhap: "+ thunhap());
       
    }

    @Override
    public float thunhap() {
        float thunhap;
        thunhap = luong + (hesoluong * luong);
        return thunhap;
    }
}
