
package javaapplication2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DanhSachVanPhong {
List<Vanphong> dsvp = new ArrayList<>();   
private int n;

    public List<Vanphong> getDsvp() {
        return dsvp;
    }

    public void setDsvp(List<Vanphong> dsvp) {
        this.dsvp = dsvp;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }
    
    
    public  void Nhap() {
        boolean check = true;
        int id;
        for (int i = 0; i < n; i++) {
            System.out.println("Nhan vien thu: "+(i+1));
            do {
                System.out.println("Nhap vao id: ");
                id = new Scanner(System.in).nextInt();
                for (int j = 0; j < dsvp.size(); j++) {
                    if (dsvp.get(j).getMa() == id) {
                        check = false;
                        break;
                    }
                }
                if (check == false) {
                    System.out.println("Ma da bi trung !!!");
                }
            } while (check == false);
            Vanphong vp = new Vanphong();
            vp.setMa(id);
            vp.nhap();
            dsvp.add(vp);
        }

    }
    
       public void Xuat() {
        for (int i = 0; i < dsvp.size(); i++) {
            dsvp.get(i).in();
        }
    }


}
