package javaapplication2;

import static java.nio.file.Files.list;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.List;
import java.util.Scanner;

public class main {

    static List<Congnhan> congNhan = new ArrayList<Congnhan>();
    static List<Vanphong> vanPhong = new ArrayList<Vanphong>();
    static List<Quanly> quanLy = new ArrayList<Quanly>();

    public static boolean test_id_vp(int id) {
        if (vanPhong.size() == 0) {

            return true;

        } else {
            return false;
        }

    }

    public static void main(String[] args) {
        int n;

        System.out.println("so nhan vien van phong");
        int vp = new Scanner(System.in).nextInt();
        System.out.println("so cong nhan");
        int cn = new Scanner(System.in).nextInt();
        System.out.println("So nhan vien quan ly");
        int ql = new Scanner(System.in).nextInt();
        do {
            System.out.println("menu");
            System.out.println("1. Nhap danh sach nhan vien van phong, cong nhan, quan ly");
            System.out.println("2. Hien thi nhan vien da co trong he thong");
            System.out.println("3. Tim kiem nhan vien co thu nhap lon nhat theo gioi tinh hoac theo vi tri");
            System.out.println("4. Thoat");
            System.out.println("chon");
            n = new Scanner(System.in).nextInt();
            switch (n) {
                case 1: //nhap
                    System.out.println("Nhan vien van phong");
                    DanhSachVanPhong dsvp = new DanhSachVanPhong();
                    dsvp.setN(vp);
                    dsvp.Nhap();

                    System.out.println("Cong nhan");
                    DanhSachCongNhan dscn = new DanhSachCongNhan();
                    dscn.setN(cn);
                    dscn.Nhap();
                    
                    System.out.println("Quan ly");
                    DanhSachQuanLy dsql = new DanhSachQuanLy();
                    dsql.setN(ql);
                    dsql.Nhap();

                    break;
                case 2: //in
                    System.out.println("Danh sach nhan vien van phong");
                    DanhSachVanPhong invp = new DanhSachVanPhong();
                    invp.Xuat();

                    System.out.println("Danh sach cong nhan");
                    DanhSachCongNhan incn = new DanhSachCongNhan();
                    incn.Xuat();

                    System.out.println("Danh sach quan ly");
                    DanhSachQuanLy inql = new DanhSachQuanLy();
                    inql.Xuat();
                    break;
                case 3: //tim kiem
                    float max = 0;
                    System.out.println("Tim nhan vien co thu nhap cao nhat theo vi tri");
                    //Nhan vien van phong
                    System.out.println("Nhan vien van phong: ");
                    for (int i = 0; i < vanPhong.size(); i++) {
                        if (vanPhong.get(i).thunhap() > max) {
                            max = vanPhong.get(i).thunhap();
                        }
                    }

                    for (int i = 0; i < vanPhong.size(); i++) {
                        if (vanPhong.get(i).thunhap() == max) {
                            vanPhong.get(i).in();
                        }
                    }
                    //cong nhan
                    System.out.println("Cong nhan: ");

                    for (int i = 0; i < congNhan.size(); i++) {
                        if (congNhan.get(i).thunhap() > max) {
                            max = congNhan.get(i).thunhap();
                        }
                    }

                    for (int i = 0; i < congNhan.size(); i++) {
                        if (congNhan.get(i).thunhap() == max) {
                            congNhan.get(i).in();
                        }
                    }
                    //quan ly
                    System.out.println("Quan ly: ");

                    for (int i = 0; i < quanLy.size(); i++) {
                        if (quanLy.get(i).thunhap() > max) {
                            max = quanLy.get(i).thunhap();
                        }
                    }

                    for (int i = 0; i < quanLy.size(); i++) {
                        if (quanLy.get(i).thunhap() == max) {
                            quanLy.get(i).in();
                        }
                    }

                    break;
                case 4: //thoat
                    break;
            }
        } while (n != 4);

    }
}
