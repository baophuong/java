/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DanhSachCongNhan {

  

    private List<Congnhan> list = new ArrayList<>();
    private int n;

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public List<Congnhan> getList() {
        return list;
    }

    public void setList(List<Congnhan> list) {
        this.list = list;
    }

    public  void Nhap() {
        boolean check = true;
        int id;
        for (int i = 0; i < n; i++) {
             System.out.println("Cong nhan thu " + (i + 1));
            do {
                System.out.println("Nhap vao id: ");
                id = new Scanner(System.in).nextInt();
                for (int j = 0; j < list.size(); j++) {
                    if (list.get(j).getMa() == id) {
                        check = false;
                        break;
                    }
                }
                if (check == false) {
                    System.out.println("Ma da bi trung !!!");
                }
            } while (check == false);
            Congnhan cn = new Congnhan();
            cn.setMa(id);
            cn.nhap();
            list.add(cn);
        }

    }

    public void Xuat() {
        for (int i = 0; i < list.size(); i++) {
            list.get(i).in();
        }
    }

}
