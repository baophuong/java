package javaapplication2;

import java.util.Scanner;

public class Quanly extends Nhansu {

    private int sonhanvien;

    public int getSonhanvien() {
        return sonhanvien;
    }

    public void setSonhanvien(int sonhanvien) {
        this.sonhanvien = sonhanvien;
    }

    @Override
    public void nhap() {
        super.nhap();
        System.out.println("Nhap so nhan vien: ");
        sonhanvien = new Scanner(System.in).nextInt();
    }

    @Override
    public void in() {
        super.in();
        System.out.println("So nhan vien: " + sonhanvien);
        System.out.println("Thu nhap: "+thunhap());
        
    }

    @Override
    public float thunhap() {
        float thunhap;
        int phucap;
        if (sonhanvien < 10) {
            phucap = 500;
        } else if ((sonhanvien >= 10) || (sonhanvien <= 20)) {
            phucap = 1000;
        } else {
            phucap = 2000;
        }
        thunhap = luong + phucap;

        return thunhap;
    }
}
