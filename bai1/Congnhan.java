package javaapplication2;

import java.util.Scanner;

public class Congnhan extends Nhansu {

    private int ngaycong;

    public int getNgaycong() {
        return ngaycong;
    }

    public void setNgaycong(int ngaycong) {
        this.ngaycong = ngaycong;
    }

    @Override
    public void nhap() {
        super.nhap();
        System.out.println("So ngay cong");
        ngaycong = new Scanner(System.in).nextInt();
    }

    @Override
    public void in() {
        super.in();
        System.out.println("So ngay cong: " + ngaycong);
        System.out.println("Thu nhap: "+thunhap());
      

    }

    @Override
    public float thunhap() {
        float thunhap;
        thunhap = luong + (ngaycong / 26) * luong;
        return thunhap;
    }
    

}
